The SNAPSHOT build from `${gitRef}` with POM version [`${project.version}`](https://repository.apache.org/#nexus-search;gav~org.apache.directory.scimple~~${project.version}~~) has been deployed ✅

Changes in this build since the last release:
