Release of tag `${gitRef}` with POM version `${project.version}` failed ❌

You may need to clean up the following:
- The [Nexus staging repository](https://repository.apache.org/#stagingRepositories) (empty if Maven build failed): ${nexusStagingUrl}
- The staged Source bundle: ${svnDistUrl}

To re-run the release, you will need to delete the tag and re-push it.
